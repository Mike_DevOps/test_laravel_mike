FROM composer:latest

RUN composer create-project  laravel/laravel test

CMD php ./test/artisan serve --host=0.0.0.0 --port=3000