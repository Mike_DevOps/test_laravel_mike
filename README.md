# Загрузка > Сборка > Запуск
    git clone https://gitlab.com/Mike_DevOps/test_laravel_mike \
    && cd test_laravel_mike \
    && docker build . --tag=test_laravel \
    && docker run --rm -h laravel -it -p 3000:3000 test_laravel

# Сервер доступен по адресу:
    http://127.0.0.1:3000/
